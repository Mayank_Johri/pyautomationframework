# import framework
from framework.basetests import BaseTestClass as BTC
from framework.utils import *
from qumu_tests.objects.login_base_page import LoginBasePage as LBP
from qumu_tests.objects.locators import LoginPageLocators as LPL

class LoginTests(BTC):

    def setUp(self):
        """Setup code."""
        print("Inside setup of login tests")
        self.loginPage = LBP(self.driver,
                             base_url='https://qumunity.qumu.com')
        self.loginPage.load()
        print("leaving setup of login tests")

    def test_Section_Header_Text(self):
        """Assert the section header text with the expected text."""
        expected = get_string(self.langFile,
                              "loginPage",
                              "SECTION_HEADER",
                              "en")
        self.assertEqual(expected, self.loginPage.get_section_header())

    def test_valid_login(self):
        """Assert valid login."""
        username, flag = get_testdata("test_valid_login", "user_id")
        if flag:
            password, flag = get_testdata("test_valid_login", "password")
            if flag:
                print(username, str.strip(password))
                self.loginPage.login(str.strip(username),
                                     str.strip(password))
                # Lets wait for login to complete
                assert(self.loginPage.is_logged_in())
            else:
                self.self.fail('error message: ' + password)
        else:
            self.self.fail('error message: ' + username)

    def test_click_cancel_link(self):
        """!!! Usability Issue, As it only displays blank once its clicked."""
        self.loginPage.click_cancel()
        self.driver.switch_to_default_content()
        assert(self.loginPage.is_element_exists(*LPL.LOGIN_FRAME) is False)
