import configparser
from selenium import webdriver
# import org
# import json
import os


class Driver_Details():
    """Driver Details.

    The is basis driver under test.
    """

    def __init__(self, driverName):
        """."""
        config = configparser.ConfigParser()
        confFile = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'driver_details.ini')
        print("Conf file: ", confFile)
        config.read(confFile)
        # Note: We are assuming that driver type is local and
        # not remote or mobile
        self.driverType = config[driverName]['type'].lower()
        if self.driverType == "local":
            self.driver = config[driverName]['driver']
            self.folderPath = config[driverName]['path']
            self.driverFunction = config[driverName]['driverFunction']
        else:
            print("In current version only local driver types are supported")
            exit(1)

    def getType(self):
        """."""
        return self.driverType

    def getDriver(self):
        """Used to call driver function to create the driver."""
        methodToCall = getattr(webdriver, self.driverFunction)
        return methodToCall()

    def getPath(self):
        """Return the folder Path where browser driver is stored."""
        return self.folderPath


# # def getBrowserDetails(browser):
# #     """return the """
# #     return Driver_Details(browser)
#
#
# if __name__ == '__main__':
#     d = Driver_Details("chrome").getDriver()
#     print(d)
#     d.close()
