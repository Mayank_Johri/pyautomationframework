"""
Utility file.

Contains all the common functions which are good to have and are reusable.
"""

import json
import sqlite3


def get_string(filePath, section, name, locale):
    """Return the string from strings.json file."""
    with open(filePath, mode='r') as json_file:
        json_data = json.load(json_file)
    return json_data[section][name][locale]


def get_testdata(testname, name):
    """."""
    try:
        conn = sqlite3.connect("qumu_tests/data/test_data.sqlite3")
        c = conn.cursor()
        c.execute("select value from test_data where "
                  "testcase_id='{0}' and name='{1}'".format(testname, name))
        data = c.fetchone()[0]
        flag = True
    except Exception as e:
        data = e
        flag = False
    return data, flag

if __name__ == '__main__':
    print(get_testdata("login_1", "user_id"))
