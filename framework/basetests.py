import unittest

from framework.driver_details import Driver_Details


class BaseTestClass(unittest.TestCase):

    def __init__(self, testName, browser):
        """."""
        super(BaseTestClass, self).__init__(testName)
        print("Selecting Browser: ", browser)
        self.browser = Driver_Details(browser)
        self.driver = self.browser.getDriver()
        self.langFile = "qumu_tests/data/strings.json"

    def setUp(self):
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.close()
