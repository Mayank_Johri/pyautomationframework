"""Base Page.

This is the base page which defines attributes and methods that all
other pages will share.
"""
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from qumu_tests.objects.locators import TopMenu


class BasePage(object):
    """Base Page Class.

    This is the base page which defines attributes and methods that all
    other pages will share.
    """

    def __init__(self, driver):
        """Initialization code."""
        self.driver = driver
        self.driver.implicitly_wait(5)
        self.timeout = 30

    # NOTE: This function needs to be moved to qumu_tests
    def is_logged_in(self):
        """Check if user is logged in or not"""
        return self.is_element_exists(*TopMenu.USER_DROPDOWN)

    # *** Common Functions ***#
    def open(self, url):
        """Visit the page base_url + url."""
        self.driver.get(url)

    def send_text(self, locator, text):
        return self.driver.find_element(*locator).send_keys(text)

    def click_element(self, *locator):
        self.driver.find_element(*locator).click()

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def find_elements(self, *locator):
        return self.driver.find_elements(*locator)

    def get_titile(self):
        return self.driver.title

    def get_url(self):
        return self.driver.current_url()

    def get_checkbox_status(self, *locator):
        return True if (self.find_element(*locator).is_selected()) else False

    def wait_for_element(self, *locator):
        return WebDriverWait(self.driver,
                             self.timeout).until(
                                 lambda self: self.find_element(*locator)
                                 )

    def is_element_exists(self, *locator):
        try:
            self.find_element(*locator)
        except NoSuchElementException:
            return False
        return True

    def switch_to_frame(self, *locator):
        self.driver.switch_to.frame(self.driver.find_element(*locator))

    def switch_to_default(self):
        self.driver.switch_to.default_content()
