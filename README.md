# pyAutomationFramework, A Basic Functional Testing Framework
---
Version: 0.0.1 {Pre-ALPHA}

This framework is developed with change in mind. Main features are as follows:

- Dynamic selection of drivers using driver_details.ini file.
- Uses page object model
- Test data stored in db
- basic support for multiple language using strings.json file

## Future Enhancements
- Browser Profile
- Mobile and remote drivers support
- Allure reporting
- tests progress
-  Allure Reporting
- Step definiation
- Screenshot of failures & success
- user friendly way to provide test data and runbook, such as using an excel sheet.
- proper error handling in framework
- multi testcase execution using selenium grid
- proper multilingual support


## Usage
The tests are selected using `modules` section of runbook.ini file, multiple ones can be seperated using `,`. Similarly browsers can be selected using browsers

Drivers can be defined using driver_details.ini file and test data is stored in sqlite file `qumu_tests\data\test_data.sqlite3` file.

multi language strings are stored in `qumu_tests\data\strings.json` file.
